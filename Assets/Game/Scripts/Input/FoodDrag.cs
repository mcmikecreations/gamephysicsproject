using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MassObject))]
public class FoodDrag : MonoBehaviour
{
    private Vector3 offset;
    private float zCoord;
    private MassObject massObject;

    private void Awake()
    {
        massObject = GetComponent<MassObject>();
    }

    private void OnMouseDown()
    {
        zCoord = Camera.main.WorldToScreenPoint(massObject.Position).z;
        offset = massObject.Position - GetMouseWorldPos();
        
    }
    private void OnMouseDrag()
    {
        massObject.Position = GetMouseWorldPos() + offset;
    }

    private Vector3 GetMouseWorldPos()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.z = zCoord;

        return Camera.main.ScreenToWorldPoint(mousePoint);
    }
}
