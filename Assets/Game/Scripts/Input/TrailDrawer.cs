using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailDrawer : MonoBehaviour
{
    public Transform trailTransform;
    public Camera camera;
    private bool trailVisible;

    public Color trailColor = new Color(1, 0, 0.38f);
    public float distanceFromCamera = 5;
    public float startWidth = 0.1f;
    public float endWidth = 0f;
    public float trailTime = 0.24f;

    void Start()
    {
        TrailRenderer trail = trailTransform.gameObject.AddComponent<TrailRenderer>();
        trail.time = -1f;
        MoveTrailToCursor(Input.mousePosition);
        trail.time = trailTime;
        trail.startWidth = startWidth;
        trail.endWidth = endWidth;
        trail.numCapVertices = 2;
        trail.sharedMaterial = new Material(Shader.Find("Unlit/Color"));
        trail.sharedMaterial.color = trailColor;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0)) trailVisible = true;
        if (Input.GetMouseButtonUp(0)) trailVisible = false;
        trailTransform.gameObject.SetActive(trailVisible);
        MoveTrailToCursor(Input.mousePosition);
    }

    void MoveTrailToCursor(Vector3 screenPosition)
    {
        trailTransform.position = camera.ScreenToWorldPoint(new Vector3(screenPosition.x, screenPosition.y, distanceFromCamera));
    }
}
