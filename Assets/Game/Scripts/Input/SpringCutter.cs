using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringCutter : MonoBehaviour
{
    [SerializeField] private MassSpringSystem springSystem;
    [SerializeField] private Camera targetCamera;

    // For spring cutter
    [SerializeField] Vector2 dragStart, dragEnd;
    [SerializeField] bool isDragging;

    // For food dragger
    [SerializeField] bool isDraggingFood;

    void Start()
    {
        isDragging = false;
        isDraggingFood = false;
        if (targetCamera == null) targetCamera = Camera.main;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartDrag(Input.mousePosition);
        }
        else if (Input.GetMouseButton(0))
        {
            KeepDrag(Input.mousePosition);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            EndDrag(Input.mousePosition);
        }
    }

    private void StartDrag(Vector2 position)
    {
        if (IsFoodSelected())
        {
            isDraggingFood = true;
        }
        dragStart = position;
        dragEnd = position;
        isDragging = true;
    }

    private void KeepDrag(Vector2 position)
    {       
        dragEnd = position;
        isDragging = true;
    }

    private void EndDrag(Vector2 position)
    {
        if (!isDraggingFood)
        {
            dragEnd = position;
            OnFinishedDrag(dragStart, dragEnd);
        }
        isDragging = false;
        isDraggingFood = false;
    }

    void OnFinishedDrag(Vector2 start, Vector2 end)
    {
        if (springSystem == null) springSystem = FindObjectOfType<MassSpringSystem>();
        bool needDelete = false;
        List<Rope> ropes = springSystem.Ropes;
        List<MassPoint> masses = springSystem.Masses;
        List<Rope> ropesToBeDeleted = new List<Rope>();
        List<MassPoint> massesToBeDeleted = new List<MassPoint>(); 

        // For auto connect, might need to change behavior here to remove the whole rope
        for (int i = 0; i < ropes.Count; i++)
        {
            Rope rope = ropes[i];
            List<Spring> springs = rope.Springs;
            
            for (int j = 0; j < springs.Count;j++)
            {
                var spring = springs[j];

                var sstart = targetCamera.WorldToScreenPoint(spring.StartPoint.transform.position);
                var send = targetCamera.WorldToScreenPoint(spring.EndPoint.transform.position);
                if (MathUtils.DoIntersect(sstart, send, start, end))
                {
                    MassPoint startPoint = rope.StartPoint;
                    MassPoint endPoint = rope.EndPoint;
                    needDelete = !(startPoint.gameObject.tag == "FixPoint" && endPoint.gameObject.tag == "Food");
                    if (needDelete)
                    {
                        if (startPoint.gameObject.tag == "FixPoint")
                        {
                            // Find the masses that should be dealed
                            for (int k = j; k < springs.Count; k++)
                            {
                                if (k != j && !massesToBeDeleted.Contains(springs[k].StartPoint))
                                {
                                    massesToBeDeleted.Add(springs[k].StartPoint);
                                }
                                if (!massesToBeDeleted.Contains(springs[k].EndPoint))
                                {
                                    massesToBeDeleted.Add(springs[k].EndPoint);
                                }
                            }

                            springs.RemoveRange(j, springs.Count - j);

                            if (springs.Count != 0)
                            {
                                rope.EndPoint = springs[springs.Count - 1].EndPoint;
                            }
                        }
                        else if (endPoint.gameObject.tag == "Food")
                        {
                            // Find the masses that should be dealed
                            for (int k = 0; k <= j; k++)
                            {
                                if (k != j && !massesToBeDeleted.Contains(springs[k].EndPoint))
                                {
                                    massesToBeDeleted.Add(springs[k].EndPoint);
                                }
                                if (!massesToBeDeleted.Contains(springs[k].StartPoint))
                                {
                                    massesToBeDeleted.Add(springs[k].StartPoint);
                                }
                            }

                            springs.RemoveRange(0, j + 1);

                            if (springs.Count != 0)
                            {
                                rope.StartPoint = springs[0].StartPoint;
                            }
                        }
                    }
                    else
                    {
                        if (j == 0)
                        {
                            // Remove the nearest section to fixpoint
                            springs.RemoveAt(j);
                            // Update rope start point
                            if (springs.Count != 0)
                            {
                                rope.StartPoint = springs[0].StartPoint;
                            }
                        }
                        else if (j == springs.Count - 1)
                        {
                            // Remove the nearest section to food
                            springs.RemoveAt(j);
                            if (springs.Count != 0)
                            {
                                rope.EndPoint = springs[springs.Count - 1].EndPoint;
                            }
                        }
                        else
                        {
                            List<Spring> springsInNewRope = new List<Spring>();
                            MassPoint fixPoint = rope.StartPoint;
                            springsInNewRope.AddRange(springs.GetRange(0, j - 1));

                            springs.RemoveRange(0, j);

                            rope.StartPoint = springs[0].StartPoint;

                            springSystem.CreateRope(fixPoint, springsInNewRope, ropes.Count + 1);
                        }
                    }

                    if (springs.Count == 0)
                    {
                        needDelete = true;
                        ropesToBeDeleted.Add(rope);
                    }

                    break;
                }

            }
        }


        // Destory
        if (needDelete)
        {
            for (int i = 0; i < massesToBeDeleted.Count; i++)
            {
                masses.Remove(massesToBeDeleted[i]);
                // Remove the MassPoint Script
                Destroy(massesToBeDeleted[i].gameObject.GetComponent<MassPoint>());
            }
            for (int i = 0; i < ropesToBeDeleted.Count; i++)
            {
                ropes.Remove(ropesToBeDeleted[i]);
                Destroy(ropesToBeDeleted[i].gameObject);
            }
        }
    }

    bool IsFoodSelected()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        // Use tag but not name here because we may have multiple food in one stage
        if (Physics.Raycast(ray,out hit) && hit.collider.gameObject.tag == "Food")
        {
            return true;
        }

        return false;
    }
}
