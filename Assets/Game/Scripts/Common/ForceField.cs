using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceField : MonoBehaviour
{
    public float maxDistance = 10f;
    public float strength = 2f;
    public bool forceEnabled = true;

    private GameSimulator simulator;

    void Awake()
    {
        simulator = GameObject.FindObjectOfType<GameSimulator>();
    }

    void Update()
    {
        if (!forceEnabled) return;

        var position = transform.position;
        var acceleration = Physics.gravity.magnitude;
        foreach (var rb in simulator.rbs)
        {
            if (rb.IsFixed) continue;

            var deltaPos = position - rb.Position;
            if (deltaPos.sqrMagnitude <= maxDistance * maxDistance)
            {
                rb.AddForce(rb.Mass * deltaPos.normalized * acceleration * strength, rb.Position);
            }
        }    
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, maxDistance);
    }
}
