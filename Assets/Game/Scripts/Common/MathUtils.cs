using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathUtils
{
    /// <summary>
    /// Given three collinear points p, q, r, the function checks if
    /// point q lies on line segment 'pr'
    /// </summary>
    static bool OnSegment(Vector2 p, Vector2 q, Vector2 r)
    {
        if (q.x <= Mathf.Max(p.x, r.x) && q.x >= Mathf.Min(p.x, r.x) &&
            q.y <= Mathf.Max(p.y, r.y) && q.y >= Mathf.Min(p.y, r.y))
            return true;

        return false;
    }

    /// <summary>
    /// To find orientation of ordered triplet (p, q, r).
    /// The function returns following values
    /// 0 --> p, q and r are collinear
    /// 1 --> Clockwise
    /// 2 --> Counterclockwise
    /// </summary>
    static int Orientation(Vector2 p, Vector2 q, Vector2 r)
    {
        // See https://www.geeksforgeeks.org/orientation-3-ordered-points/
        // for details of below formula.
        float val = (q.y - p.y) * (r.x - q.x) -
                (q.x - p.x) * (r.y - q.y);

        if (val == 0.0f) return 0; // collinear

        return (val > 0.0f) ? 1 : 2; // clock or counterclock wise
    }

    /// <summary>
    /// The main function that returns true if line segment 'p1q1'
    /// and 'p2q2' intersect.
    /// </summary>
    public static bool DoIntersect(Vector2 p1, Vector2 q1, Vector2 p2, Vector2 q2)
    {
        // Find the four orientations needed for general and
        // special cases
        int o1 = Orientation(p1, q1, p2);
        int o2 = Orientation(p1, q1, q2);
        int o3 = Orientation(p2, q2, p1);
        int o4 = Orientation(p2, q2, q1);

        // General case
        if (o1 != o2 && o3 != o4)
            return true;

        // Special Cases
        // p1, q1 and p2 are collinear and p2 lies on segment p1q1
        if (o1 == 0 && OnSegment(p1, p2, q1)) return true;

        // p1, q1 and q2 are collinear and q2 lies on segment p1q1
        if (o2 == 0 && OnSegment(p1, q2, q1)) return true;

        // p2, q2 and p1 are collinear and p1 lies on segment p2q2
        if (o3 == 0 && OnSegment(p2, p1, q2)) return true;

        // p2, q2 and q1 are collinear and q1 lies on segment p2q2
        if (o4 == 0 && OnSegment(p2, q1, q2)) return true;

        return false; // Doesn't fall in any of the above cases
    }

    public static Quaternion QuaternionMultiply(Quaternion q1, Quaternion q2)
    {
        Vector3 v1 = new Vector3(q1[0], q1[1], q1[2]);
        Vector3 v2 = new Vector3(q2[0], q2[1], q2[2]);
        Vector3 nv = v1 * q2[3] + v2 * q1[3] + Vector3.Cross(v2, v1);
        float nw = q1[3] * q2[3] - (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);

        return new Quaternion(nv.x, nv.y, nv.z, nw);
    }

    public static Quaternion QuaternionMultiplyScalar(float m, Quaternion q)
    {
        q[0] *= m;
        q[1] *= m;
        q[2] *= m;
        q[3] *= m;

        return q;
    }

    public static Quaternion QuaternionAdd(Quaternion q1, Quaternion q2)
    {
        return new Quaternion(q1[0] + q2[0], q1[1] + q2[1], q1[2] + q2[2], q1[3] + q2[3]);
    }
}
