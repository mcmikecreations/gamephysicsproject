using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody))]
public class MassObject : MonoBehaviour
{
    [SerializeField] private Vector3 velocity = Vector3.zero;
    [SerializeField] private Vector3 angularVelocity = Vector3.zero;
    [SerializeField] private List<Vector3> forces = new List<Vector3>();
    [SerializeField] private List<Vector3> forcePositions = new List<Vector3>();
    [SerializeField] private Vector3 angularMomentum = Vector3.zero;
    //[SerializeField] private Matrix4x4 invInertiaInitial = Matrix4x4.identity;
    //[SerializeField] private Matrix4x4 invInetria = Matrix4x4.identity;
    private bool isTouching;
    [SerializeField] private bool useGravity;
    [SerializeField] private bool isFixed;

    private Matrix4x4 invInertiaInitial = Matrix4x4.identity;
    private Matrix4x4 invInetria = Matrix4x4.identity;
    private Vector3 fixedPosition = Vector3.zero;
    private Quaternion fixedRotation;
    private Vector3 loosePosition = Vector3.zero;
    private Quaternion looseRotation;

    private Rigidbody rigidBody;
    private bool isInitialized;

    public float c = 0.5f;

    public static float minVelocity = 0.4f;
    public static float minMomentum = 0.66f;
    public static float friction = 0.1f;


    public void Initialize()
    {
        rigidBody = GetComponent<Rigidbody>();

        rigidBody.isKinematic = false;
        rigidBody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

        invInertiaInitial = Matrix4x4.zero;
        //loosePosition = fixedPosition = transform.localPosition;
        loosePosition = fixedPosition = transform.position;
        looseRotation = fixedRotation = transform.rotation;
        if (isFixed)
        {
            isInitialized = true;
            return;
        }

        invInertiaInitial = Matrix4x4.zero;
        if (gameObject.tag == "FixPoint" || gameObject.tag == "Food" || gameObject.tag == "InternalMass")
        {
            // For Sphere
            float value = 2 / 5 * Mass * transform.localScale.x; // A sphere which x = y = z
            invInertiaInitial[0, 0] = 12.0f * value;
            invInertiaInitial[1, 1] = 12.0f * value;
            invInertiaInitial[2, 2] = 12.0f * value;
            invInertiaInitial[3, 3] = 1;
        }
        else
        {
            // For Cube
            float xSquared = Mathf.Pow(transform.localScale[0], 2);
            float ySquared = Mathf.Pow(transform.localScale[1], 2);
            float zSquared = Mathf.Pow(transform.localScale[2], 2);
            float massInverse = 1f / Mass;
            invInertiaInitial[0, 0] = 12.0f * massInverse / (ySquared + zSquared);
            invInertiaInitial[1, 1] = 12.0f * massInverse / (xSquared + zSquared);
            invInertiaInitial[2, 2] = 12.0f * massInverse / (xSquared + ySquared);
            invInertiaInitial[3, 3] = 1;
        }
        isInitialized = true;
    }

    private void Awake()
    {
        if (!isInitialized)
        {
            Initialize();
        }
    }

    private void Update()
    {
        if(this.gameObject.tag == "Food")
        {
            if(this.transform.localPosition.y < -20)
            {
                SceneManager.LoadScene(1);
            }
        }
    }

    public void ClearForces()
    {
        forces.Clear();
        forcePositions.Clear();
    }

    //public void UpdateObject()
    //{
    //    transform.position = Position;
    //    transform.rotation = Rotation;
    //}

    public void AddForce(Vector3 force, Vector3 position)
    {
        forces.Add(force);
        forcePositions.Add(position);
    }

    // https://forum.unity.com/threads/get-vertices-of-box-collider.89301/
    private static Vector3[] GetColliderVertexPositions(BoxCollider collider)
    {
        var vertices = new Vector3[8];
        var storedRotation = collider.transform.rotation;
        // collider.transform.rotation = Quaternion.identity;

        var center = collider.center + collider.transform.position;
        var extents = new Vector3(collider.size.x, collider.size.y, collider.size.z);
        extents.Scale(collider.transform.localScale / 2f);

        vertices[0] = center + storedRotation * (extents);
        vertices[1] = center + storedRotation * new Vector3(-extents.x, extents.y, extents.z);
        vertices[2] = center + storedRotation * new Vector3(extents.x, extents.y, -extents.z);
        vertices[3] = center + storedRotation * new Vector3(-extents.x, extents.y, -extents.z);
        vertices[4] = center + storedRotation * new Vector3(extents.x, -extents.y, extents.z);
        vertices[5] = center + storedRotation * new Vector3(-extents.x, -extents.y, extents.z);
        vertices[6] = center + storedRotation * new Vector3(extents.x, -extents.y, -extents.z);
        vertices[7] = center + storedRotation * -extents;

        // collider.transform.rotation = storedRotation;
        return vertices;
    }

    private void PushOut(MassObject dynamicBody, MassObject staticBody, bool hasFriction)
    {
        Vector3 localVelocity = staticBody.transform.InverseTransformDirection(dynamicBody.Velocity);

        BoxCollider collider0 = dynamicBody.GetComponent<BoxCollider>();
        BoxCollider collider1 = staticBody.GetComponent<BoxCollider>();
        if (!collider0 || !collider1) return;

        Vector3[] pointsWorldSpace = GetColliderVertexPositions(collider0);
        Vector3[] pointsLocalSpace = pointsWorldSpace
            .Select(x => collider1.transform.InverseTransformPoint(x)).ToArray();

        for (int i = 0; i < pointsLocalSpace.Length; ++i)
        {
            Vector3 worldPoint = pointsWorldSpace[i];
            Vector3 localPoint = pointsLocalSpace[i];

            float distX = Mathf.Abs(localPoint.x);
            float distY = Mathf.Abs(localPoint.y);
            float distZ = Mathf.Abs(localPoint.z);

            const float thresholdToFloat = 1f;
            const float floatScale = 0.5f;
            const float thresholdDepthToStop = 0.6f;
            const float thresholdFloatToStop = 0.1f;

            const bool changeVelocity = true;
            const bool changePosition = true;
            const bool changeForces = false;

            if (distX >= thresholdToFloat || distY >= thresholdToFloat || distZ >= thresholdToFloat) continue;
            char max;
            if (distX >= distY && distX >= distZ) max = 'x';
            if (distY >= distZ && distY >= distX) max = 'y';
            else max = 'z';

            if (max == 'x')
            {
                if (localPoint.x >= 0f && localVelocity.x >= 0f) continue; // The object is going out anyway
                float deltaSize = thresholdToFloat;
                if (localPoint.x < 0f) deltaSize = -thresholdToFloat;
                Vector3 localEdge = new Vector3(deltaSize, localPoint.y, localPoint.z);
                Vector3 deltaWorld = collider1.transform.TransformPoint(localEdge) - collider1.transform.TransformPoint(localPoint);
                if (Vector3.Angle(-deltaWorld, Physics.gravity) >= 45f) continue;
                if (distX <= thresholdDepthToStop || Mathf.Abs(deltaSize - localPoint.x) <= thresholdFloatToStop)
                {
                    if (HasGravity) AddForce(-Physics.gravity * Mass, Position);
                    if (changeVelocity) dynamicBody.Velocity = staticBody.transform.TransformDirection(new Vector3(0f, localVelocity.y, localVelocity.z)
                        * (hasFriction ? (1f - /*friction*/0f) : 1f));
                }
                else
                {
                    //dynamicBody.Velocity = staticBody.transform.TransformDirection(new Vector3(-localVelocity.x, localVelocity.y, localVelocity.z));
                }
                if (changePosition) MoveMassObject(dynamicBody, deltaWorld * floatScale);

                if (hasFriction && changeForces)
                {
                    var force = CalculateOppositeTorqueForce(dynamicBody, worldPoint - dynamicBody.Position);
                    dynamicBody.AddForce(force, worldPoint);
                    //dynamicBody.forces.ForEach(force => force.Scale(new Vector3(1f - friction, 1f - friction, 1f - friction)));
                    //var strength = dynamicBody.forces.Sum(x => x.magnitude);
                    //dynamicBody.AddForce(-Vector3.Cross(dynamicBody.angularVelocity, worldPoint - dynamicBody.Position).normalized * strength * friction, worldPoint);
                    //dynamicBody.angularMomentum *= (1f - friction);
                }
                break;
            }
            if (max == 'y')
            {
                if (localPoint.y >= 0f && localVelocity.y >= 0f) continue; // The object is going out anyway

                float deltaSize = thresholdToFloat;
                if (localPoint.y < 0f) deltaSize = -thresholdToFloat;
                Vector3 localEdge = new Vector3(localPoint.x, deltaSize, localPoint.z);
                Vector3 deltaWorld = collider1.transform.TransformPoint(localEdge) - collider1.transform.TransformPoint(localPoint);
                if (Vector3.Angle(-deltaWorld, Physics.gravity) >= 45f) continue;
                if (distY <= thresholdDepthToStop || Mathf.Abs(deltaSize - localPoint.y) <= thresholdFloatToStop)
                {
                    if (HasGravity) AddForce(-Physics.gravity * Mass, Position);
                    if (changeVelocity) dynamicBody.Velocity = staticBody.transform.TransformDirection(new Vector3(localVelocity.x, 0f, localVelocity.z)
                        * (hasFriction ? (1f - /*friction*/0f) : 1f));
                }
                else
                {
                    //dynamicBody.Velocity = staticBody.transform.TransformDirection(new Vector3(localVelocity.x, -localVelocity.y, localVelocity.z));
                }
                if (changePosition) MoveMassObject(dynamicBody, deltaWorld * floatScale);

                if (hasFriction && changeForces)
                {
                    var force = CalculateOppositeTorqueForce(dynamicBody, worldPoint - dynamicBody.Position);
                    dynamicBody.AddForce(force, worldPoint);
                    //dynamicBody.forces.ForEach(force => force.Scale(new Vector3(1f - friction, 1f - friction, 1f - friction)));
                    //var strength = dynamicBody.forces.Sum(x => x.magnitude);
                    //dynamicBody.AddForce(-Vector3.Cross(dynamicBody.angularVelocity, worldPoint - dynamicBody.Position).normalized * strength * friction, worldPoint);
                    //dynamicBody.angularMomentum *= (1f - friction);
                }
                break;
            }
            if (max == 'z')
            {
                if (localPoint.z >= 0f && localVelocity.z >= 0f) continue; // The object is going out anyway
                float deltaSize = thresholdToFloat;
                if (localPoint.z < 0f) deltaSize = -thresholdToFloat;
                Vector3 localEdge = new Vector3(localPoint.x, localPoint.y, deltaSize);
                Vector3 deltaWorld = collider1.transform.TransformPoint(localEdge) - collider1.transform.TransformPoint(localPoint);
                if (Vector3.Angle(-deltaWorld, Physics.gravity) >= 45f) continue;
                if (distZ <= thresholdDepthToStop || Mathf.Abs(deltaSize - localPoint.z) <= thresholdFloatToStop)
                {
                    if (HasGravity) AddForce(-Physics.gravity * Mass, Position);
                    if (changeVelocity) dynamicBody.Velocity = staticBody.transform.TransformDirection(new Vector3(localVelocity.x, localVelocity.y, 0f)
                        * (hasFriction ? (1f - /*friction*/0f) : 1f));
                }
                else
                {
                    //dynamicBody.Velocity = staticBody.transform.TransformDirection(new Vector3(localVelocity.x, localVelocity.y, -localVelocity.z));
                }
                if (changePosition) MoveMassObject(dynamicBody, deltaWorld * floatScale);

                if (hasFriction && changeForces)
                {
                    var force = CalculateOppositeTorqueForce(dynamicBody, worldPoint - dynamicBody.Position);
                    dynamicBody.AddForce(force, worldPoint);
                    //dynamicBody.forces.ForEach(force => force.Scale(new Vector3(1f - friction, 1f - friction, 1f - friction)));
                    //var strength = dynamicBody.forces.Sum(x => x.magnitude);
                    //dynamicBody.AddForce(-Vector3.Cross(dynamicBody.angularVelocity, worldPoint - dynamicBody.Position).normalized * strength * friction, worldPoint);
                    //dynamicBody.angularMomentum *= (1f - friction);
                }
                break;
            }
        }
    }

    private Vector3 CalculateOppositeTorqueForce(MassObject dynamicBody, Vector3 deltaPos)
    {
        CalculateTorque(dynamicBody, out var torque, out var totalForces);
        // -torque * friction = i x (deltaPos.y * z - deltaPos.z * y) + j x (deltaPos.z * x - deltaPos.x * z) + k x (deltaPos.x * y - deltaPos.y * x)
        // deltaPos.y * z - deltaPos.z * y = (-torque * friction).x
        // deltaPos.z * x - deltaPos.x * z = (-torque * friction).y
        // deltaPos.x * y - deltaPos.y * x = (-torque * friction).z
        // or
        // 0 * x - deltaPos.z * y + deltaPos.y * z = (-torque * friction).x
        // deltaPos.z * x + 0 * y - deltaPos.x * z = (-torque * friction).y
        // -deltaPos.y * x + deltaPos.x * y + 0 * z = (-torque * friction).z
        // Ax = b
        // x = A^-1 * b
        Matrix4x4 A = new Matrix4x4(
            new Vector4(0f, -deltaPos.z, deltaPos.y, 0f),
            new Vector4(deltaPos.z, 0f, -deltaPos.x, 0f),
            new Vector4(-deltaPos.y, deltaPos.x, 0f, 0f),
            new Vector4(0f, 0f, 0f, 1f)
            );
        Vector3 oppositeTorque = -torque * friction;
        Vector4 b = new Vector4(oppositeTorque.x, oppositeTorque.y, oppositeTorque.z, 1f);
        Vector4 x = A.inverse * b;
        return new Vector3(x.x, x.y, x.z);
    }

    private void MoveMassObject(MassObject mass, Vector3 deltaPosition)
    {
        mass.Position += deltaPosition;
        mass.transform.position = mass.Position;
    }

    void CalculateTorque(MassObject mass, out Vector3 torque, out Vector3 totalForces)
    {
        torque = Vector3.zero;
        totalForces = Vector3.zero;

        for (int i = 0; i < mass.forces.Count; i++)
        {
            totalForces += mass.forces[i];
            torque += Vector3.Cross(mass.forcePositions[i] - mass.loosePosition, mass.forces[i]);
        }
    }

    public void Simulate(float timestep)
    {
        if (isFixed)
        {
            ClearForces();
            transform.position = fixedPosition;
            transform.rotation = fixedRotation;
            return;
        }
        if (Mass <= 0f)
        {
            return;
        }

        Vector3 totalForces = Vector3.zero;
        Vector3 torque = Vector3.zero;
        //get the applied forces (fi)
        //get the applied forces points (xi)
        // calculate torque by cross product of xi and fi for the whole interval i

        if (isTouching && velocity.magnitude < minVelocity)
        {
            velocity = Vector3.zero;
            //HasGravity = false;
        }
        if (!isTouching)
        {
            //HasGravity = true;
        }

        if (HasGravity)
        {
            // Add gravity
            AddForce(Physics.gravity * Mass, loosePosition);
        }
        CalculateTorque(this, out torque, out totalForces);

        Vector3 acceleration = totalForces / Mass;

        // leap frog
        velocity += acceleration * timestep;
        if (velocity.sqrMagnitude < minVelocity * minVelocity)
        {
            velocity = Vector3.zero;
        }
        loosePosition += velocity * timestep;

        transform.position = loosePosition;
        if (this.gameObject.GetComponent<MassPoint>() != null && this.gameObject.GetComponent<MassPoint>().tag != "InternalMass")
        {
            // Update rigid body
            angularMomentum += timestep * torque;

            Quaternion rotation = looseRotation;
            Matrix4x4 rot = Matrix4x4.Rotate(rotation);
            Matrix4x4 rotT = rot.transpose;
            InvInertia = rot * invInertiaInitial * rotT;
            angularVelocity = InvInertia * angularMomentum;
            if (isTouching && angularMomentum.magnitude < minMomentum)
            {
                angularMomentum = Vector3.zero;
                angularVelocity = Vector3.zero;
            }
            //transform.rotation = Quaternion.Normalize(transform.rotation * quaternion_scalar_multiplication((Quaternion.Euler(angularVelocity[0], angularVelocity[1], angularVelocity[2]) * transform.rotation), (timestep * 0.5f)));
            //transform.rotation = transform.rotation * timestep / 2 * t * r;
            Quaternion angularVq = new Quaternion(angularVelocity[0], angularVelocity[1], angularVelocity[2], 0);
            looseRotation = MathUtils.QuaternionAdd(transform.rotation, MathUtils.QuaternionMultiplyScalar(timestep / 2, MathUtils.QuaternionMultiply(angularVq, transform.rotation)));
            transform.rotation = looseRotation;
        }

        ClearForces();
    }

    void HandleCollision(Collision collision, bool hasFriction)
    {
        MassObject body0 = gameObject.GetComponent<MassObject>();
        Rigidbody rb0 = gameObject.GetComponent<Rigidbody>();
        MassObject body1 = collision.gameObject.GetComponent<MassObject>();
        Rigidbody rb1 = collision.gameObject.GetComponent<Rigidbody>();
        if (!body0 || !rb0 || !body1 || !rb1) return;
        if (body0.gameObject.tag == "InternalMass" && body1.gameObject.tag == "InternalMass" ||
            body0.gameObject.tag == "InternalMass" && body1.gameObject.tag == "FixPoint" ||
            body0.gameObject.tag == "InternalMass" && body1.gameObject.tag == "Food" ||
            body0.gameObject.tag == "Food" && body1.gameObject.tag == "InternalMass" ||
            body0.gameObject.tag == "FixPoint" && body1.gameObject.tag == "InternalMass") return;

        ContactPoint[] collisionPoints = collision.contacts;
        ContactPoint collisionPoint = collisionPoints[0];
        Vector3 collisionNormal = collisionPoint.normal;

        Vector3 xaWorld = collisionPoint.point - body0.Position;
        Vector3 xbWorld = collisionPoint.point - body1.Position;

        Matrix4x4 rotation0 = Matrix4x4.Rotate(body0.Rotation);
        Matrix4x4 rotation1 = Matrix4x4.Rotate(body1.Rotation);
        Matrix4x4 rotationTranspose0 = rotation0.transpose;
        Matrix4x4 rotationTranspose1 = rotation1.transpose;

        Matrix4x4 currentInertiaTensorInverse0 = rotation0 * body0.InvInertiaInitial * rotationTranspose0;
        Matrix4x4 currentInertiaTensorInverse1 = rotation1 * body1.InvInertiaInitial * rotationTranspose1;
        Vector3 angularVel_A = currentInertiaTensorInverse0 * body0.AngularMomentum;
        Vector3 angularVel_B = currentInertiaTensorInverse1 * body1.AngularMomentum;

        Vector3 velocityA = body0.Velocity + Vector3.Cross(angularVel_A, xaWorld);
        Vector3 velocityB = body1.Velocity + Vector3.Cross(angularVel_B, xbWorld);

        Vector3 relVelocity = velocityA - velocityB;
        float relVelonNormal = Vector3.Dot(relVelocity, collisionNormal);

        if (relVelonNormal < 0.0f)
        {
            c = 0.5f;

            float InvMa = body0.IsFixed ? 0 : (1.0f / Mass);
            float InvMb = body1.IsFixed ? 0 : (1.0f / rb1.mass);

            float numerator = -(1.0f + c) * relVelonNormal;
            float inverseMasses = InvMa + InvMb;
            Vector3 rma = Vector3.Cross(currentInertiaTensorInverse0 * Vector3.Cross(xaWorld, collisionNormal), xaWorld);
            Vector3 rmb = Vector3.Cross(currentInertiaTensorInverse1 * Vector3.Cross(xbWorld, collisionNormal), xbWorld);
            float rmab = Vector3.Dot(rma + rmb, collisionNormal);
            float denominator = inverseMasses + rmab;
            float impulse = numerator / denominator;

            Vector3 impulseNormal = impulse * collisionNormal;
            if (!body0.IsFixed)
            {
                body0.Velocity += impulseNormal * InvMa;
                body0.AngularMomentum += Vector3.Cross(xaWorld, impulseNormal);
            }
            if (!body1.IsFixed)
            {
                body1.Velocity -= impulseNormal * InvMb;
                body1.AngularMomentum -= Vector3.Cross(xbWorld, impulseNormal);
            }
        }

        if (!body0.IsFixed && body1.IsFixed)
        {
            PushOut(body0, body1, hasFriction);
        }
        if (!body1.IsFixed && body0.IsFixed)
        {
            PushOut(body1, body0, hasFriction);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.GetComponent<MassObject>().IsFixed) isTouching = true;
        HandleCollision(collision, false);
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.transform.GetComponent<MassObject>().IsFixed) isTouching = true;
        HandleCollision(collision, true);
    }

    private void OnCollisionExit(Collision collision)
    {
        isTouching = false;
    }

    #region Properties
    public Vector3 Velocity { get => velocity; set => velocity = value; }
    public Vector3 AngularVelocity { get => angularVelocity; set => angularVelocity = value; }
    public List<Vector3> Forces { get => forces; set => forces = value; }
    public List<Vector3> ForcePositions { get => forcePositions; set => forcePositions = value; }
    public Vector3 AngularMomentum { get => angularMomentum; set => angularMomentum = value; }
    public Matrix4x4 InvInertiaInitial { get => invInertiaInitial; set => invInertiaInitial = value; }
    public Matrix4x4 InvInertia { get => invInetria; set => invInetria = value; }
    public bool IsFixed { get => isFixed; set => isFixed = value; }
    public bool HasGravity {
        //get => rigidBody.useGravity;
        //set => rigidBody.useGravity = value;
        // Don't use rigidBody's useGravity since it has its own logic to handle gravity
        get => useGravity;
        set => useGravity = value;
    }
    public float Mass { get => rigidBody.mass; set => rigidBody.mass = value; }
    public Vector3 Position
    {
        get => isFixed ? fixedPosition : loosePosition;
        set
        {
            if (isFixed) fixedPosition = value;
            else loosePosition = value;
        }
    }
    public Quaternion Rotation
    {
        get => isFixed ? fixedRotation : looseRotation;
        set
        {
            if (isFixed) fixedRotation = value;
            else looseRotation = value;
        }
    }
    #endregion
}
