using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum UpdateType
{
    Update,
    FixedUpdate,
    SingleTimeStep,
}

public class GameSimulator : MonoBehaviour
{
    [SerializeField] private UpdateType updateType = UpdateType.FixedUpdate;
    [SerializeField] private float timestep = 0.01f;
    [SerializeField] private float minRigidBodyVelocity = 0.1f;
    [SerializeField] private float minRigidBodyMomentum = 0.1f;
    [SerializeField] private float minRigidBodyFriction = 0.1f;

    private MassSpringSystem mss;
    //private RigidBodySystem rbs;

    public List<MassObject> rbs;

    private void Awake()
    {
        mss = GetComponent<MassSpringSystem>();
        //rbs = GetComponent<RigidBodySystem>();
        rbs = GameObject.FindGameObjectsWithTag("RB").Select(x => x.GetComponent<MassObject>()).ToList();

        UpdateMassObjectFields();
    }

    void UpdateMassObjectFields()
    {
        MassObject.minVelocity = minRigidBodyVelocity;
        MassObject.minMomentum = minRigidBodyMomentum;
        MassObject.friction = minRigidBodyFriction;
    }

    private void FixedUpdate()
    {
        if (updateType == UpdateType.FixedUpdate)
        {
            float timestep = Time.fixedDeltaTime;
            Simulate(timestep);
        }
    }

    private void Update()
    {
        UpdateMassObjectFields();

        if (updateType != UpdateType.FixedUpdate)
        {
            float localTimestep = updateType == UpdateType.Update ? Time.deltaTime : timestep;
            Simulate(localTimestep);
        }
    }

    private void Simulate(float totalTimestep)
    {
        // Target timestep is totalTimestep, single timestep is timestep
        /*
        float totalTime = 0f;
        do
        {
            Dictionary<int, MassObject> objects = new Dictionary<int, MassObject>();

            if (mss)
            {
                mss.Simulate(timestep);
                foreach (var massPoint in mss.Masses)
                {
                    if (!objects.ContainsKey(massPoint.MassObject.GetInstanceID()))
                    {
                        objects.Add(massPoint.MassObject.GetInstanceID(), massPoint.MassObject);
                    }
                }
            }
            
            if (rbs)
            {
                // rbs.Simulate(timestep);
                foreach (var rigidBody in rbs.RigidBodies)
                {
                    if (!objects.ContainsKey(rigidBody.MassObject.GetInstanceID()))
                    {
                        objects.Add(rigidBody.MassObject.GetInstanceID(), rigidBody.MassObject);
                    }
                }
            }

            foreach (var massPoint in objects.Values)
            {
                massPoint.Simulate(timestep);
                massPoint.ClearForces();
            }

            totalTime += timestep;
        } while (totalTime < totalTimestep);
        */
        if (mss) mss.Simulate(totalTimestep);

        for(int i = 0; i < rbs.Count; i++)
        {
            MassObject rb = rbs[i].GetComponent<MassObject>();
            rb.Simulate(totalTimestep);
        }
        
        //rbs.Simulate(totalTimestep);
    }

    #region Properties
    public UpdateType UpdateType { get => updateType; set => updateType = value; }
    public float TimeStep { get => timestep; }

    public MassSpringSystem MSS { get => mss; }
    #endregion
}
