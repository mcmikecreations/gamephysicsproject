using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GameSimulator))]
public class Stage1 : MonoBehaviour
{
    private GameSimulator gameSimulator;
    public GameObject[] fixPoints;
    public GameObject food;

    private void Awake()
    {
        gameSimulator = GetComponent<GameSimulator>();
    }

    void Start()
    {
        // initialize the springs and masses
        var mss = gameSimulator.MSS;

        fixPoints = GameObject.FindGameObjectsWithTag("FixPoint");

        if (food == null) food = GameObject.FindWithTag("Food");
        MassPoint foodMass = food.AddComponent<MassPoint>();

        mss.AddMass(foodMass);

        for (int i = fixPoints.Length - 1; i >= 0; i--)
        {
            // Caculate how many mass do we need for the rope
            GameObject fixPoint = fixPoints[i];
            MassPoint fixMass = fixPoint.AddComponent<MassPoint>();
            mss.AddMass(fixMass);
            if (fixPoint.name == "FixPoint4")
            {
                fixMass.AddCanAutoConnectConfig(food.GetInstanceID(), true);
                continue;
            }

            mss.CreateRope(fixMass, foodMass, i);
        }
    }
}
