using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Stage2 : MonoBehaviour
{
    [SerializeField] private GameObject fixPointPrefab;
    [SerializeField] private GameObject rigidBodyPrefab;
    [SerializeField] private GameObject forceInfluencePrefab;
    [SerializeField] private int gridLength;
    [SerializeField] private float cellLength;

    [SerializeField] private bool spawnFixPoints;
    [SerializeField] private bool spawnRigidBodies;

    [SerializeField] private float springStiffness = 10.0f;
    [SerializeField] private float springInitialLength = 3.0f;
    [SerializeField] private float initialPushStrength = 3f;

    public ForceField[] forceFields;
    public MassPoint[] fixedMassPoints;
    
    private GameSimulator gameSimulator;

    private void Awake()
    {
        gameSimulator = GetComponent<GameSimulator>();
        if (forceFields == null) forceFields = new ForceField[0];
    }

    void Start()
    {
        StartCoroutine(AddBodies());
    }

    void SpawnObjects()
    {
        if (spawnFixPoints)
        {
            MassObject[,] masses = new MassObject[gridLength, gridLength];
            for (int i = 0; i < gridLength; ++i)
            {
                for (int j = 0; j < gridLength; ++j)
                {
                    var position = new Vector3(i * cellLength, 0f, j * cellLength);
                    var mass = GameObject.Instantiate(fixPointPrefab, position, Quaternion.identity);

                    var massObject = masses[i, j] = mass.GetComponent<MassObject>();
                    massObject.HasGravity = true;
                    massObject.IsFixed = i == 0 || i == gridLength - 1 || j == 0 || j == gridLength - 1;
                    massObject.Position = position;

                    MassPoint fixMass = mass.AddComponent<MassPoint>();
                    gameSimulator.MSS.AddMass(fixMass);

                    mass.tag = "RB";
                    gameSimulator.rbs.Add(massObject);
                }
            }

            ConnectMasses(masses);
        }

        if (spawnRigidBodies)
        {
            Vector3 center = new Vector3((gridLength / 2f) * cellLength, 5f, (gridLength / 2f) * cellLength);
            for (int i = 1; i < gridLength - 1; ++i)
            {
                for (int j = 1; j < gridLength - 1; ++j)
                {
                    var position = new Vector3(i * cellLength, 5f, j * cellLength);
                    var mass = GameObject.Instantiate(rigidBodyPrefab, position, Quaternion.identity);

                    var massObject = mass.GetComponent<MassObject>();
                    massObject.HasGravity = true;
                    massObject.IsFixed = false;
                    massObject.Position = position;
                    massObject.Rotation = Quaternion.Euler(40, 40, 40);
                    massObject.Velocity = (center - position).normalized * initialPushStrength;

                    //var massPoint = mass.AddComponent<MassPoint>();
                    foreach (var fixPoint in fixedMassPoints)
                    {
                        if (fixPoint == null) continue;
                        fixPoint.AddCanAutoConnectConfig(mass.GetInstanceID(), true);
                    }

                    mass.transform.rotation = massObject.Rotation;
                    mass.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    mass.tag = "RB";
                    gameSimulator.rbs.Add(massObject);
                }
            }
        }

        spawnFixPoints = false;
    }

    void ConnectMasses(MassObject[,] masses)
    {
        Vector3 center = new Vector3((gridLength / 2f) * cellLength, 5f, (gridLength / 2f) * cellLength);

        for (int i = 0; i < gridLength - 1; ++i)
        {
            for (int j = 1; j < gridLength - 1; ++j)
            {
                gameSimulator.MSS.CreateRope(masses[i, j].GetComponent<MassPoint>(), masses[i + 1, j].GetComponent<MassPoint>(), i * gridLength + j, true);
            }
        }

        for (int i = 0; i < gridLength - 1; ++i)
        {
            for (int j = 1; j < gridLength - 1; ++j)
            {
                gameSimulator.MSS.CreateRope(masses[j, i].GetComponent<MassPoint>(), masses[j, i + 1].GetComponent<MassPoint>(), i * gridLength + j, true);
            }
        }

        foreach (var rope in gameSimulator.MSS.Ropes)
        {
            foreach (var spring in rope.Springs)
            {
                float maxDist = Mathf.Max((spring.EndPoint.MassObject.Position - center).sqrMagnitude, (spring.StartPoint.MassObject.Position - center).sqrMagnitude);
                spring.Stiffness = Mathf.Sqrt(maxDist) * springStiffness;
                spring.InitialLength = springInitialLength;
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(AddBodies());
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }

    void ToggleFields()
    {
        foreach (var field in forceFields)
        {
            if (field == null) continue;
            CreateForceFieldInfluence(field);
            ToggleForceField(field);
        }
    }

    IEnumerator AddBodies()
    {
        SpawnObjects();
        yield return new WaitForSeconds(2.75f);
        ToggleFields();
        yield return new WaitForSeconds(1.5f);
        ToggleFields();
    }

    void CreateForceFieldInfluence(ForceField forceField)
    {
        if (forceField.transform.childCount == 1)
        {
            var influenceGO = Instantiate(forceInfluencePrefab, forceField.transform);
        }
    }

    void ToggleForceField(ForceField forceField)
    {
        forceField.forceEnabled = !forceField.forceEnabled;
        var childTransform = forceField.transform.GetChild(forceField.transform.childCount - 1);
        childTransform.localScale = Vector3.one * forceField.maxDistance * 2f;
        childTransform.gameObject.SetActive(forceField.forceEnabled);
    }
}
