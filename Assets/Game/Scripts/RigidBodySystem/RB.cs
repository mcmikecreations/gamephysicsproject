using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MassObject))]
public class RB : MonoBehaviour
{
    private MassObject massObject;

    #region Setter and Getter
    public MassObject MassObject
    {
        get => massObject;
    }
    #endregion

    private void Awake()
    {
        massObject = GetComponent<MassObject>();

        var rbs = GameObject.FindObjectOfType<RigidBodySystem>();
        if (rbs)
        {
            rbs.AddRigidBody(this);
        }
    }

    /// <summary>
    /// Call only outside of the MassSimulator
    /// </summary>
    /// <param name="timestep"></param>
    
    public void Simulate(float timestep)
    {
        if (massObject.IsFixed || massObject.Mass <= 0f) return;

        Vector3 totalforces = new Vector3(0.0f, 0.0f, 0.0f);
        Vector3 torque = new Vector3(0.0f, 0.0f, 0.0f);
        //get the applied forces (fi)
        //get the applied forces points (xi)
        // calculate torque by cross product of xi and fi for the whole interval i
        for (int i = 0; i < massObject.Forces.Count; i++)
        {
            totalforces = totalforces + massObject.Forces[i];
            torque = torque + Vector3.Cross(massObject.ForcePositions[i], massObject.Forces[i]);
        }
        if (massObject.HasGravity) totalforces += Physics.gravity * massObject.Mass;

        massObject.Position += timestep * massObject.Velocity;
        massObject.Velocity += timestep * totalforces / gameObject.GetComponent<Rigidbody>().mass;
        massObject.AngularMomentum += timestep * torque;

        Quaternion rotation = massObject.Rotation;
        Matrix4x4 rot = Matrix4x4.Rotate(rotation);
        Matrix4x4 rotT = rot.transpose;
        Matrix4x4 currentInertiaTensorInverse = rot * massObject.InvInertiaInitial * rotT;
        massObject.AngularVelocity = currentInertiaTensorInverse * massObject.AngularMomentum;

        Quaternion angularVq = new Quaternion(massObject.AngularVelocity[0], massObject.AngularVelocity[1], massObject.AngularVelocity[2], 0);
        massObject.Rotation = MathUtils.QuaternionAdd(
            rotation,
            MathUtils.QuaternionMultiplyScalar(
                timestep / 2,
                MathUtils.QuaternionMultiply(angularVq, rotation)
            )
        );

        massObject.ClearForces();
    }

    
    void OnCollisionStay(Collision collision)
    {
        RB body0 = gameObject.GetComponent<RB>();
        Rigidbody rb0 = gameObject.GetComponent<Rigidbody>();
        RB body1 = collision.gameObject.GetComponent<RB>();
        Rigidbody rb1 = collision.gameObject.GetComponent<Rigidbody>();
        if (!body0 || !rb0 || !body1 || !rb1) return;

        ContactPoint[] collisionPoints = collision.contacts;
        ContactPoint collisionPoint = collisionPoints[0];
        Vector3 collisionNormal = collisionPoint.normal;

        Vector3 xaWorld = collisionPoint.point - body0.massObject.Position;
        Vector3 xbWorld = collisionPoint.point - body1.massObject.Position;

        Matrix4x4 rotation0 = Matrix4x4.Rotate(body0.massObject.Rotation);
        Matrix4x4 rotation1 = Matrix4x4.Rotate(body1.massObject.Rotation);
        Matrix4x4 rotationTranspose0 = rotation0.transpose;
        Matrix4x4 rotationTranspose1 = rotation1.transpose;

        Matrix4x4 currentInertiaTensorInverse0 = rotation0 * body0.massObject.InvInertiaInitial * rotationTranspose0;
        Matrix4x4 currentInertiaTensorInverse1 = rotation1 * body1.massObject.InvInertiaInitial * rotationTranspose1;
        Vector3 angularVel_A = currentInertiaTensorInverse0 * body0.massObject.AngularMomentum;
        Vector3 angularVel_B = currentInertiaTensorInverse1 * body1.massObject.AngularMomentum;

        Vector3 velocityA = body0.massObject.Velocity + Vector3.Cross(angularVel_A, xaWorld);
        Vector3 velocityB = body1.massObject.Velocity + Vector3.Cross(angularVel_B, xbWorld);

        Vector3 relVelocity = velocityA - velocityB;
        float relVelonNormal = Vector3.Dot(relVelocity, collisionNormal);
        if (relVelonNormal < 0.0f)
        {
            massObject.c = 0.5f;

            float InvMa = body0.massObject.IsFixed ? 0 : (1.0f / gameObject.GetComponent<Rigidbody>().mass);
            float InvMb = body1.massObject.IsFixed ? 0 : (1.0f / collision.gameObject.GetComponent<Rigidbody>().mass);

            float numerator = -(1.0f + massObject.c) * relVelonNormal;
            float inverseMasses = InvMa + InvMb;
            Vector3 rma = Vector3.Cross(currentInertiaTensorInverse0 * Vector3.Cross(xaWorld, collisionNormal), xaWorld);
            Vector3 rmb = Vector3.Cross(currentInertiaTensorInverse1 * Vector3.Cross(xbWorld, collisionNormal), xbWorld);
            float rmab = Vector3.Dot(rma + rmb, collisionNormal);
            float denominator = inverseMasses + rmab;
            float impulse = numerator / denominator;

            Vector3 impulseNormal = impulse * collisionNormal;
            if (!body0.massObject.IsFixed)
            {
                body0.massObject.Velocity += impulseNormal * InvMa;
                body0.massObject.AngularMomentum += Vector3.Cross(xaWorld, impulseNormal);
            }
            if (!body1.massObject.IsFixed)
            {
                body1.massObject.Velocity -= impulseNormal * InvMb;
                body1.massObject.AngularMomentum -= Vector3.Cross(xbWorld, impulseNormal);
            }

            // TODO: should we really clear all forces, including ones from springs?
            massObject.ClearForces();
        }
    }
}