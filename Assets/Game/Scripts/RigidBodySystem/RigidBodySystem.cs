using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidBodySystem : MonoBehaviour
{
    
    private List<RB> rigidBodies = new List<RB>();

    #region Setter and Getter
    public void AddRigidBody(RB rigidbody)
    {
        if (rigidBodies == null) rigidBodies = new List<RB>();
        rigidBodies.Add(rigidbody);
    }

    public List<RB> RigidBodies
    {
        get => rigidBodies;
        set => rigidBodies = value;
    }
    #endregion

    public void Simulate(float timestep)
    {
        UpdateRigidBodies(timestep);
    }

    void UpdateRigidBodies(float timestep)
    {
        for (int i = 0; i < rigidBodies.Count; i++)
        {
            rigidBodies[i].Simulate(timestep);
        }
    }

    public void ClearRigidBodiesForces()
    {
        for (int i = 0; i < rigidBodies.Count; i++)
        {
            rigidBodies[i].MassObject.ClearForces();
        }
    }
    
}
