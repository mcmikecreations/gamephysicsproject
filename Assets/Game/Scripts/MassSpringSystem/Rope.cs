using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rope : MonoBehaviour
{
    private LineRenderer lineRenderer;
    private List<Spring> springs;
    private float sectionLength = 1.0f;
    private float massSize = 0.1f;

    private MassPoint startPoint;
    private MassPoint endPoint;

    private float ropeMass = 1.0f;
    
    void Start()
    {
        gameObject.AddComponent<LineRenderer>();
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Legacy Shaders/Particles/Alpha Blended Premultiply"));
        lineRenderer.startColor = lineRenderer.endColor = Color.grey;
        lineRenderer.startWidth = 0.05f;
        lineRenderer.endWidth = 0.05f;
    }

    #region Setter and Getter
    public void AddSpring(Spring sp)
    {
        if (springs == null) springs = new List<Spring>();
        springs.Add(sp);
    }

    public List<Spring> Springs
    {
        get => springs;
        set => springs = value;
    }

    public float SectionLength
    {
        get => sectionLength;
    }

    public float MassSize
    {
        get => massSize;
    }

    public MassPoint StartPoint
    {
        get => startPoint;
        set => startPoint = value;
    }

    public MassPoint EndPoint
    {
        get => endPoint;
        set => endPoint = value;
    }

    public float RopeMass
    {
        get => ropeMass;
    }

    #endregion

    public void Draw()
    {
        if (lineRenderer == null)
            return;

        Vector3[] positions = new Vector3[springs.Count + 1];

        for (int i = 0; i < springs.Count; i++)
        {
            if (i != springs.Count - 1)
            {
                positions[i] = springs[i].StartPoint.transform.position;
            } else
            {
                positions[i] = springs[i].StartPoint.transform.position;
                positions[i + 1] = springs[i].EndPoint.transform.position;
            }
        }

        lineRenderer.positionCount = positions.Length;
        lineRenderer.SetPositions(positions);
    }

}
