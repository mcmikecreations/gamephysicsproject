using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MassSpringSystem : MonoBehaviour
{
    private List<Rope> ropes = new List<Rope>();
    private List<MassPoint> masses = new List<MassPoint>();

    public float autoConnectDistance = 1.5f;

    #region Setter and Getter
    public void AddRope(Rope rope)
    {
        if (ropes == null) ropes = new List<Rope>();
        ropes.Add(rope);
    }

    public List<Rope> Ropes
    {
        get => ropes;
        set => ropes = value;
    }

    public void AddMass(MassPoint mass)
    {
        if (masses == null) masses = new List<MassPoint>();
        masses.Add(mass);
    }

    public List<MassPoint> Masses
    {
        get => masses;
        set => masses = value;
    }
    #endregion

    public void Simulate(float timestep)
    {
        // Using heap frog
        ComputeElasticForces();

        UpdateMasses(timestep);

        AutoConnect();

        DrawRopes();
    }

    void AutoConnect()
    {
        // Get Foods
        GameObject[] foods;
        foods = GameObject.FindGameObjectsWithTag("Food");
        // Get FixPoints
        GameObject[] fixPoints;
        fixPoints = GameObject.FindGameObjectsWithTag("FixPoint");

        for (int i = 0; i < foods.Length; i++)
        {
            MassPoint food = foods[i].GetComponent<MassPoint>();
            if (!food) continue;

            for (int j = 0; j < fixPoints.Length; j++)
            {
                MassPoint fixPoint = fixPoints[j].GetComponent<MassPoint>();
                if (!fixPoint) continue;

                if (fixPoint.CanAutoConnectConfig(food.gameObject.GetInstanceID()))
                {
                    float distance = (food.MassObject.Position - fixPoint.MassObject.Position).magnitude;

                    if (distance < autoConnectDistance)
                    {
                        CreateRope(fixPoint, food, ropes.Count + 1);
                    }
                }
            }
        }
    }

    void ComputeElasticForces()
    {
        for (int i = 0; i < ropes.Count; i++)
        {
            List<Spring> springs = ropes[i].Springs;
            for (int j = 0; j < springs.Count; j++)
            {
                springs[j].ComputeElasticForce();
            }
        }
    }

    void UpdateMasses(float timestep)
    {
        for (int i = 0; i < masses.Count; i++)
        {
            masses[i].Simulate(timestep);
        }
    }

    public void ClearMassesForces()
    {
        for (int i = 0; i < masses.Count; i++)
        {
            masses[i].MassObject.ClearForces();
        }
    }

    void DrawRopes()
    {
        for (int i = 0; i < ropes.Count; i++)
        {
            ropes[i].Draw();
        }
    }
    public void CreateRope(MassPoint fixPoint, List<Spring> springs, int idx)
    {
        if (fixPoint != null && springs.Count > 0)
        {
            GameObject ropeObject = new GameObject("Rope" + idx);
            Rope rope = ropeObject.AddComponent<Rope>();

            rope.StartPoint = fixPoint;
            rope.Springs = springs;
            rope.EndPoint = springs[springs.Count - 1].EndPoint;

            ropes.Add(rope);
        }
    }
    public void CreateRope(MassPoint fixPoint, MassPoint food, int idx, bool isDirectConnection = false)
    {
        GameObject ropeObject = new GameObject("Rope" + idx);
        Rope rope = ropeObject.AddComponent<Rope>();

        Vector3 disV = fixPoint.MassObject.Position - food.MassObject.Position;
        float distance = disV.magnitude;
        int numOfMass = (int)Mathf.Floor(distance / rope.SectionLength) - 1;
        float massSize = rope.MassSize;
        //float eachMass = rope.RopeMass / numOfMass;

        List<Spring> ropeSprings = new List<Spring>();
        List<MassPoint> ropeMass = new List<MassPoint>();

        ///if (numOfMass <= 0 || isDirectConnection)
        //{
        Spring spring = fixPoint.gameObject.AddComponent<Spring>();
        spring.StartPoint = fixPoint;
        spring.EndPoint = food;
        ropeSprings.Add(spring);

        rope.Springs = ropeSprings;
        rope.StartPoint = fixPoint;
        rope.EndPoint = food;
        ropes.Add(rope);
        //}
        /*
        else
        {
            // Create the rope masses
            float eachMass = rope.RopeMass / numOfMass;
            for (int i = 0; i < numOfMass; i++)
            {
                var item = GameObject.CreatePrimitive(PrimitiveType.Sphere).AddComponent<MassPoint>();
                item.name = "RopeMass_" + idx.ToString() + "_" + i.ToString();
                item.tag = "InternalMass";

                // Disable rigidBody's use Gravity!
                Rigidbody rb = item.gameObject.GetComponent<Rigidbody>();
                rb.useGravity = false;
                item.MassObject.HasGravity = true;

                item.transform.localScale = Vector3.one * massSize;
                item.transform.localPosition = item.MassObject.Position = fixPoint.MassObject.Position - disV.normalized * (i + 1);
                item.transform.localRotation = item.MassObject.Rotation = Quaternion.identity;
                item.MassObject.Mass = eachMass;
                //item.MassObject.Mass = 0.1f;
                item.GetComponent<Renderer>().enabled = false;
                item.MassObject.Initialize();

                masses.Add(item);
                ropeMass.Add(item);
            }
            // Create the springs inside the rope
            for (int j = 0; j < numOfMass + 1; j++)
            {
                Spring spring;
                if (j == 0)
                {
                    // start is fix point
                    spring = ropeMass[0].gameObject.AddComponent<Spring>();
                    spring.StartPoint = fixPoint;
                    spring.EndPoint = ropeMass[0];
                }
                else if (j == numOfMass)
                {
                    // end is food
                    spring = ropeMass[j - 1].gameObject.AddComponent<Spring>();
                    spring.StartPoint = ropeMass[j - 1].GetComponent<MassPoint>();
                    spring.EndPoint = food;
                }
                else
                {
                    spring = ropeMass[j - 1].gameObject.AddComponent<Spring>();
                    spring.StartPoint = ropeMass[j - 1].GetComponent<MassPoint>();
                    spring.EndPoint = ropeMass[j].GetComponent<MassPoint>();
                }
                ropeSprings.Add(spring);
            }
            rope.Springs = ropeSprings;
            rope.StartPoint = fixPoint;
            rope.EndPoint = food;
            ropes.Add(rope);
        }
    */
        fixPoint.AddCanAutoConnectConfig(food.gameObject.GetInstanceID(), false);
    }
}