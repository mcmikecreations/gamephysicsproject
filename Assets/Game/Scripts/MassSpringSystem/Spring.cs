using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spring : MonoBehaviour
{
    private MassPoint start;
    private MassPoint end;
    [SerializeField] private float initialLength = 3.0f;
    [SerializeField] private float stiffness = 10.0f;
    //private float springDamping = 2.0f;

    #region Public Getter and Setter
    public MassPoint StartPoint
    {
        get => start;
        set => start = value;
    }
    
    public MassPoint EndPoint
    {
        get => end;
        set => end = value;
    }

    public float InitialLength
    {
        get => initialLength;
        set => initialLength = value;
    }

    public float Stiffness
    {
        get => stiffness;
        set => stiffness = value;
    }
    #endregion

    public void ComputeElasticForce()
    {
        Vector3 diff = start.MassObject.Position - end.MassObject.Position;
        Vector3 forceA = -stiffness * diff.normalized * (diff.magnitude - initialLength);
        Vector3 forceB = -forceA;

        //start.MassObject.AddForce(force, Vector3.zero);
        //end.MassObject.AddForce(-force, Vector3.zero);

        //Damping force applied on end
        //Vector3 dampingForce = - springDamping * diff.normalized * Vector3.Dot(start.MassObject.Velocity - end.MassObject.Velocity, diff.normalized);
        //forceB -= dampingForce;

        // Add elastic force from spring
        start.MassObject.AddForce(forceA, start.MassObject.gameObject.transform.position);
        end.MassObject.AddForce(forceB, end.MassObject.gameObject.transform.position);
    }
}
