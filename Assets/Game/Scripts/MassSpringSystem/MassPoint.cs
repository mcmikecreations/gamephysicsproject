using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MassObject))]
public class MassPoint : MonoBehaviour
{
    private MassObject massObject;
    // Property
    private float damping;
    private Dictionary<int, bool> canAutoConnectConfigs;

    private void Awake()
    {
        massObject = GetComponent<MassObject>();
        damping = 0.3f;
    }

    #region Setter and Getter
    public MassObject MassObject
    {
        get => massObject;
    }

    public float Damping
    {
        get => damping;
    }

    public void AddCanAutoConnectConfig(int foodId, bool canConnectConfig)
    {
        // This property is only used by fixPoints
        if (canAutoConnectConfigs == null) canAutoConnectConfigs = new Dictionary<int, bool>();
        if (canAutoConnectConfigs.ContainsKey(foodId))
        {
            canAutoConnectConfigs[foodId] = canConnectConfig;
        } else
        {
            canAutoConnectConfigs.Add(foodId, canConnectConfig);
        }
    }

    public bool CanAutoConnectConfig(int foodId)
    {
        canAutoConnectConfigs.TryGetValue(foodId, out bool canConnectConfig);
        return canConnectConfig;
    }
    #endregion

    public void Simulate(float timestep)
    {
        if (massObject.IsFixed)
        {
            massObject.ClearForces();
            massObject.Simulate(timestep);
            return;
        }
        if (massObject.Mass == 0.0f) return;
        Vector3 force = Vector3.zero;

        // Air resistance
        //force -= massObject.Velocity.normalized * massObject.Velocity.sqrMagnitude;
        //Vector3 airResistance = - massObject.Velocity.normalized * massObject.Velocity.sqrMagnitude;

        // Damping
        //force -= massObject.Velocity * damping;
        Vector3 dampingForce =  -massObject.Velocity * damping;

        //massObject.AddForce(force, Vector3.zero);
        // Add damping and air resistance force related to velocity
        //massObject.AddForce(airResistance, massObject.gameObject.transform.position);
        massObject.AddForce(dampingForce, massObject.gameObject.transform.position);

        massObject.Simulate(timestep);
    }
}
